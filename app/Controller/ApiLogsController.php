<?php

class ApiLogsController extends AppController {

	public function beforeFilter() {
		parent::beforeFilter();
	}

/**
 * Method index to get the saved logs in serialized form
 *
 * @return void
 */
	public function index() {
		if ($this->request->is('get')) {
			$limit = configure::read('DefaultLimit');

			if (isset($this->request->query) && !empty($this->request->query)) {
				$filterData = $this->__apiLogQueryFilter();
			}

			$params = array(
				'order' => array('_id' => -1),
				'limit' => $limit
			);
			$results = $this->ApiLog->find('all', $params);
			$this->response->statusCode(configure::read('HTTP_OK'));
	    	$this->set(array(
	            'response' => $results,
	            '_serialize' => array('response')
	        ));
		} else {
			$this->response->statusCode(configure::read('HTTP_BAD_REQUEST'));
			$this->autoRender = false;
		}
	}

	private function __apiLogQueryFilter() {
		$filterData = $this->request->query;
		if (isset($filterData['limit']) && !empty($filterData['limit'])) {
			$limit = $filterData['limit'];
		}
	}

/**
 * Method add to save api logs into the mongodb database and set the headers as response
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			if (isset($this->request->data) && !empty($this->request->data)) {
				$this->ApiLog->create();
				if ($this->ApiLog->save($this->request->data)) {
					$this->response->statusCode(configure::read('HTTP_CREATED'));
	   				$this->autoRender = false;
				}
			}
		} else {
			$this->response->statusCode(configure::read('HTTP_BAD_REQUEST'));
			$this->autoRender = false;
		}
	}
}
