<?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('Controller', 'Controller');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package		app.Controller
 * @link		http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {
	public $components = array(
		'Session',
		'RequestHandler',
	    'Auth' => array(
	        'authenticate' => array(
	            'Basic' => array(
	                'fields' => array('username' => 'email', 'password' => 'password')
	            )
	        )
	    )
	);

	function beforeFilter() {
		if (!$this->authenticate()) {
			//$this->response->statusCode(configure::read('HTTP_UNAUTHORIZED'));
			header('HTTP/1.1 401 Unauthorized', true, 401);
			exit();
		}
	}

/**
 * Method authenticate to authenticate user on each call
 *
 * @return true if user authenticated successfully.
 */
	public function authenticate() {
		$this->loadModel('User');
		$this->User->useDbConfig = 'mongousers';
		$this->Session->delete('Auth');
		if ($this->Auth->login()) {
            return true;
        }
        return false;
	}
}
