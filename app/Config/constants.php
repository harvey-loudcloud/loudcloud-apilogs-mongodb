<?php
$config['HTTP_OK'] = 200;
$config['HTTP_CREATED'] = 201;
$config['HTTP_ACCEPTED'] = 202;
$config['HTTP_NO_CONTENT'] = 204;

$config['HTTP_MOVED_PERMANENTLY'] = 300;
$config['HTTP_MOVED_TEMPORARILY'] = 301;
$config['HTTP_SEE_OTHER'] = 302;
$config['HTTP_NOT_MODIFIED'] = 304;

$config['HTTP_BAD_REQUEST'] = 400;
$config['HTTP_UNAUTHORIZED'] = 401;
$config['HTTP_FORBIDDEN'] = 403;
$config['HTTP_NOT_FOUND'] = 404;

$config['HTTP_INTERNAL_SERVER_ERROR'] = 500;
$config['HTTP_NOT_IMPLEMENTED'] = 501;
$config['HTTP_BAD_GATEWAY'] = 502;
$config['HTTP_SERVICE_UNAVAILABLE'] = 503;

$config['DefaultLimit'] = 50;