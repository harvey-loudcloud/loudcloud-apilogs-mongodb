<?php

class ApiLog extends AppModel {

    public $name = 'ApiLog';

    public function afterFind($results, $primary = true) {
        foreach ($results as $key => $val) {
            if (isset($val['ApiLog']['id'])) {
                $results[$key]['ApiLog']['modified'] = $val['ApiLog']['modified']->sec;
                $results[$key]['ApiLog']['created'] = $val['ApiLog']['created']->sec;
            }
        }
        return $results;
    }
}